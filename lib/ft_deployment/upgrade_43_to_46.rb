require 'capistrano'

class CapeZUpgrade43to46
  def self.load_into(cap_config)
    cap_config.load do
      before "upgrade:mysqldump", "upgrade:display_timestamp"
      after "upgrade:mysqldump", "upgrade:display_timestamp"
      before "upgrade:upgrade_43to44", "upgrade:display_timestamp"
      after "upgrade:upgrade_43to44", "upgrade:display_timestamp"
      before "upgrade:upgrade_44to45", "upgrade:display_timestamp"
      after "upgrade:upgrade_44to45", "upgrade:display_timestamp"
      before "upgrade:upgrade_45to46", "upgrade:display_timestamp"
      after "upgrade:upgrade_45to46", "upgrade:display_timestamp"
      before "upgrade:upgrade_43to46", "upgrade:display_timestamp"
      after "upgrade:upgrade_43to46", "upgrade:display_timestamp"

      # Rollback code after rollback database:
      after "upgrade:rollback", "deploy:rollback"

      # Upgrade tasks
      namespace :upgrade do

        desc "Update MySQL database structure"
        task :mysqldump, :roles => :master do
          # Backup MySQL Data
          run "cd #{current_path} && mysql -h #{mysql_server} -u #{mysql_user} -p#{mysql_pwd} #{mysql_db} -e 'truncate table ezsession;'"
          run "cd #{current_path} && mkdir -p #{upgrade_mysqldump_directory}"
          run "cd #{current_path} && mysqldump --single-transaction --quick -h #{mysql_server} -u #{mysql_user} -p#{mysql_pwd} #{mysql_db} > #{upgrade_mysqldump_directory}/#{application}.sql"
        end

        desc "Upgrade 4.3 => 4.4"
        task :upgrade_43to44, :roles => :master do
          run "cd #{current_path} && mysql -h #{mysql_server} -u #{mysql_user} -p#{mysql_pwd} #{mysql_db} < update/database/mysql/4.4/dbupdate-4.3.0-to-4.4.0.sql"
          run "cd #{current_path} && php update/common/scripts/4.4/updatesectionidentifier.php"
        end

        desc "Upgrade 4.4 => 4.5"
        task :upgrade_44to45, :roles => :master do
          run "cd #{current_path} && mysql -h #{mysql_server} -u #{mysql_user} -p#{mysql_pwd} #{mysql_db} < update/database/mysql/4.5/dbupdate-4.4.0-to-4.5.0.sql"
          run "cd #{current_path} && php update/common/scripts/4.5/updatesectionidentifier.php"
        end

        desc "Upgrade 4.5 => 4.6"
        task :upgrade_45to46, :roles => :master do
          run "cd #{current_path} && mysql -h #{mysql_server} -u #{mysql_user} -p#{mysql_pwd} #{mysql_db} < update/database/mysql/4.6/dbupdate-4.5.0-to-4.6.0.sql"
          run "cd #{current_path} && php update/common/scripts/4.6/updateordernumber.php"
          run "cd #{current_path} && php update/common/scripts/4.6/removetrashedimages.php -n"
        end

        task :reindex_solr, :roles => :master do
          run "cd #{current_path} && php extension/ezfind/bin/php/updatesearchindexsolr.php -s site_admin --clean --php-exec=/usr/bin/php"
        end

        task :upgrade_43to45 do
          upgrade_43to44
          upgrade_44to45
          deploy.clear_local_caches
          deploy.clear_global_caches
        end

        task :upgrade_44to46 do
          upgrade_44to45
          upgrade_45to46
          deploy.clear_local_caches
          deploy.clear_global_caches
        end

        task :upgrade_43to46 do
          upgrade_43to44
          upgrade_44to45
          upgrade_45to46
          deploy.clear_local_caches
          deploy.clear_global_caches
          generate_sitemaps
        end

        desc "'Manually' runs the cron job to ensure sitemaps are present"
        task :generate_sitemaps, :roles => :master do
          run "cd #{current_path} && php runcronjobs.php -q -s site_admin googlesitemaps"
        end

        task :rollback, :roles => :master do
          # Rollback MySQL Data
          run "cd #{current_path} && mysql -h #{mysql_server} -u #{mysql_user} -p#{mysql_pwd} #{mysql_db} < #{upgrade_mysqldump_directory}/#{application}.sql"
        end

        desc "Display timestamp"
        task :display_timestamp do
          system(  "date +%H:%M:%S" )
        end
      end
    end
  end
end

if Capistrano::Configuration.instance
  CapeZUpgrade43to46.load_into(Capistrano::Configuration.instance)
end
