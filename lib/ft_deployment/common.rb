require 'capistrano'

class CapeZSettings
  def self.load_into(cap_config)
    cap_config.load do
      set :scm, :git
      set :deploy_via, :remote_cache
      set :copy_strategy, :checkout
      set :keep_releases, 3
      set :use_sudo, false
      set :copy_compression, :bz2
      set :git_enable_submodules, 1
      set :user, "deploy"
    end
  end
end

if Capistrano::Configuration.instance
  CapeZSettings.load_into(Capistrano::Configuration.instance)
end
